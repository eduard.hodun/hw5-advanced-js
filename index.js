
const request = async (method, url, body = null) => {
    const headers = {
        'Content-Type': 'application/json',
    };

    try {
        const response = await axios({
            url: url,
            headers: headers,
            method: method,
            data: body,
        });

        if (method !== 'DELETE') {
            return response.data;
        } else {
            return response;
        }
        
    } catch (error) {
        console.error(error);
        throw error;
    }
};


const deletePost = async (postId, urlPost) => {
    try {
        const response = await request('DELETE', `${urlPost}/${postId}`);

        if (response && response.status === 200) {
            const removePost = document.querySelector(`.card__post-${postId}`);
            if (removePost) {
                removePost.remove();
            }
        } else {
            console.error('Error');
        }
    } catch (error) {
        console.error(error);
    }
};



class Card {
    constructor(urlUser, urlPost) {
        this.urlUser = urlUser;
        this.urlPost = urlPost;
    }

    async listCard() {
        try {
            const users = await request('GET', this.urlUser);
            console.log(users);
            const posts = await request('GET', this.urlPost);
            console.log(posts);


            const twitterPostContainer = document.querySelector('.container');

            const contentCards = posts.map(
                ({ id, userId, title, body }) =>
                    
                    
                `<div class = "card__post card__post-${id}">
                    <div class = "card">
                        <div class = "card__header">
                            <div class = "card__header_title">
                                <img class = "card__logo" src = "./img/user2.png" alt = "User">
                                <h1>${users.find(user => user.id === userId).name}</h1>
                                <h3>${users.find(user => user.id === userId).email}</h3>
                            </div>
                            
                        </div>


                        <h2>${title}</h2>
                        <p>${body}</p>

                        <button class = "card__post_btn" data-id = ${id}>Delete</button>
                    </div>
                   
                </div>`
            );

            twitterPostContainer.innerHTML = contentCards.join('');


            const deleteButtons = document.querySelectorAll('.card__post_btn');
            deleteButtons.forEach(button => {
                button.addEventListener('click', async () => {
                    const postId = button.getAttribute('data-id');
                    await deletePost(postId, this.urlPost);
                });
            });
        } catch (error) {
            console.error(error);
        }
    }
}

const card = new Card('https://ajax.test-danit.com/api/json/users', 'https://ajax.test-danit.com/api/json/posts');

card.listCard();

